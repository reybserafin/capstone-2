const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require("./routes/orderRoutes");

const port = 4000;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/products', productRoutes);
app.use('/users', userRoutes);
app.use("/orders", orderRoutes);

mongoose.connect(
  "mongodb+srv://admin:genericpassword1@cluster0.49qws.mongodb.net/capstone-2?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.once('open', () => {
    console.log('Connected to Mongo DB Atlas')
});

app.listen(process.env.PORT || port, () => {
    console.log(`Listening on port ${process.env.PORT || port}`);
})
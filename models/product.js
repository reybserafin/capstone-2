const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  productName: {
    type: String,
    required: [true, "Product Name is required"],
  },
  productCode: {
    type: String,
    required: [true, "stockNumber is required"],
  },
  description: {
    type: String,
    required: [true, "Description is required"],
  },
  color: {
    type: String,
    required: [true, "Color is required"],
  },
  price: {
    type: Number,
    required: [true, "Price is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },  
});

module.exports = mongoose.model('Product', productSchema);;
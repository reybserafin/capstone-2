const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");
const orderController = require("../controllers/orderController");

router.get("/all", (req, res) => {
  let token = auth.decode(req.headers.authorization);

  if (token.isAdmin === true) {
    orderController.getOrders().then((result) => res.send(result));
  } else {
    res.send({ auth: "Not an admin" });
  }
});

router.post("/checkOut", (req, res) => {
  const data = {
    totalAmount: req.body.totalAmount,
    userId: auth.decode(req.headers.authorization).id,
    productId: req.body.productId,
  };
  let token = auth.decode(req.headers.authorization);

  if (token.isAdmin === false || token.isAdmin === true) {
    orderController
      .checkOut(data)
      .then((resultFromCheckOut) => res.send(resultFromCheckOut));
  }
});

router.get("/:userId", (req, res) => {
  orderController
    .getByUserId(req.params)
    .then((resultFromGetSpecific) => res.send(resultFromGetSpecific));
});

module.exports = router;

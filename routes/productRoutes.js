const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");
const productController = require("../controllers/productController");

//add a new Product
router.post("/addProduct", auth.verify, (req, res) => {
  let token = auth.decode(req.headers.authorization)
	
  if (token.isAdmin === true) {
    productController
      .addProduct(req.body)
      .then((resultFromAddProduct) => res.send(resultFromAddProduct));
  } else {
    res.send({ auth: "Not an admin" });
  }
});

router.get("/allProducts", (req, res) => {
  productController
    .getProducts()
    .then((resultFromGet) => res.send(resultFromGet));
});

router.get("/all", (req, res) => {
  productController
    .getAllActive()
    .then((resultFromGet) => res.send(resultFromGet));
});

//update Product information (Admin Only)
router.post('/:productId', auth.verify, (req, res) => {
  let token = auth.decode(req.headers.authorization)
	
  if (token.isAdmin === true) {
    productController
    .updateProduct(req.params, req.body)
    .then(resultFromUpdate => res.send(resultFromUpdate))
  } else {
    res.send({ auth: "Not an admin" });
  }
})

//get specific Product by ID
router.get("/:productId", (req, res) => {
  productController
    .getByProductId(req.params)
    .then((resultFromGetSpecific) => res.send(resultFromGetSpecific));
});



router.delete("/:productId", auth.verify, (req, res) => {
  let token = auth.decode(req.headers.authorization);

  if (token.isAdmin === true) {
    productController
      .archiveProduct(req.params)
      .then((resultFromArchive) => res.send(resultFromArchive));
  } else {
    res.send({ auth: "Not an admin" });
  }
});


module.exports = router;

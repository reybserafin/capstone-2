const express = require('express');
const router = express.Router();
const auth = require('../auth');
const userController = require('../controllers/userController');
// const orderController = require('../controllers/orderController')

router.post('/checkEmail', (req, res) => {
    userController.checkEmail(req.body)
    .then(resultFromEmailExists => res.send(resultFromEmailExists))
});

router.post('/register', (req, res) => {
    userController.registerUser(req.body)
    .then(resultFromRegister => res.send(resultFromRegister))
});

router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromLogin) => res.send(resultFromLogin));
});

router.get("/all", (req, res) => {
  userController.getUsers().then((result) => res.send(result));
});

router.post('/:userId', auth.verify, (req, res) => {
  let token = auth.decode(req.headers.authorization)
	
  if (token.isAdmin === true) {
    userController
    .updateUser(req.params, req.body)
    .then(resultFromUpdate => res.send(resultFromUpdate))
  } else {
    res.send({ auth: "Not an admin" });
  }
})

module.exports = router
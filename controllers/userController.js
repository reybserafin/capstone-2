const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order')
const auth = require('../auth');
const bcrypt = require('bcrypt');

module.exports.registerUser = (body) => {

    let newUser = new User({
        userName: body.userName,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        mobileNo: body.mobileNo
    })

    return newUser.save().then((user, error) => {
        if (error) {
            return false;
        } else {
            return true
        }
    })
}

module.exports.checkEmail = (body) => {
  return User.find({ email: body.email }).then((result) => {
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

module.exports.loginUser = (body) => {
  return User.findOne({ email: body.email }).then((result) => {
    if (result === null) {
      return false; //user doesn't exist
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        body.password,
        result.password
      );

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result.toObject()) };
      } else {
        return false;
      }
    }
  });
};

module.exports.getProfile = (userId) => {
  return User.findById(userId).then((result) => {
    result.password = undefined;
    return result;
  });
};

module.exports.updateUser = (params) => {
  let updatedUser = {
    isAdmin: true,
  };

  return User.findByIdAndUpdate(params.userId, updatedUser).then(
    (user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// module.exports.createOrder = (body) => {

//   let newOrder = new Order({
//       userName: body.userName,
//       email: body.email,
//       password: bcrypt.hashSync(body.password, 10),
//       mobileNo: body.mobileNo
//   })

//   return newUser.save().then((user, error) => {
//       if (error) {
//           return false;
//       } else {
//           return true
//       }
//   })
// }

// module.exports.checkOut = async (data) => {
  
//   let productSaveStatus = await Product.findById(data.productId).then(
//     (product) => {
//       product.purchases.push({ userId: data.userId});
//       return product.save().then((product, err) => {
//         if (err) {
//           return false;
//         } else {
//           return true;
//         }
//       });
//     }
//   );

//   let userSaveStatus = await User.findById(data.userId).then((user) => {
//     user.orders.push({ productId: data.productId});
//     return user.save().then((user, err) => {
//       if (err) {
//         return false;
//       } else {
//         return true;
//       }
//     });
//   });

//   if (productSaveStatus && userSaveStatus) {
//     return true;
//   } else {
//     return false;
//   }

  
  
// };




module.exports.getUsers = () => {
	return User.find().then(result => {
		return result;
	})
}

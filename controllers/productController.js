const User = require('../models/user');
const Order = require("../models/order");
const Product = require("../models/product");
const auth = require("../auth");

//add Product
module.exports.addProduct = (body) => {
  let newProduct = new Product({
    productName: body.productName,
    productCode: body.productCode,
    description: body.description,
    color: body.color,
    price: body.price,
  });

  return newProduct.save().then((user, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

//update Product
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		productName: body.productName,
    productCode: body.productCode,
    description: body.description,
    color: body.color,
    price: body.price,
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

module.exports.getByProductId = (params) => {
  return Product.findById(params.productId).then((result) => {
    return result;
  });
};

module.exports.getProducts = () => {
	return Product.find().then(resultFromGet => {
    return resultFromGet;
  });
}

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.archiveProduct = (params) => {
  let archivedProduct = {
    isActive: false,
  };

  return Product.findByIdAndUpdate(params.productId, archivedProduct).then(
    (product, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};
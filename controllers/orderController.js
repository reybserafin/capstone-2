const User = require("../models/user");
const Product = require("../models/product");
const Order = require("../models/order");
const auth = require("../auth");

module.exports.getOrders = () => {
  return Order.find().then((result) => {
    return result;
  });
};

module.exports.checkOut = (body) => {
  let newOrder = new Order({
    totalAmount: body.totalAmount,
    userId: body.userId,
    productId: body.productId,
  });

  return newOrder.save().then((order, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

module.exports.getByUserId = (params) => {
  return Order.find({ userId: params.userId }).then((result) => {
    return result;
  });
};
